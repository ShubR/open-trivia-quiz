import './Home.css';
import { useNavigate } from "react-router-dom";

const Home = () => {

  const navigate = useNavigate();

  const startQuiz = () => {
    navigate('/quiz');
  };

  return (
    <div className="home-container">
        <button className='start-btn' onClick={startQuiz}>Start Quiz <span className="arrow">&gt;</span></button>
    </div>
  )
};

export default Home;



