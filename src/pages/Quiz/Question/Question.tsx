import { useEffect, useState } from "react";
import "./Question.css";

type QuestionProps = {
  selectOption: (option: string) => void;
  question: string;
  options: string[];
  correct_answer: string;
  index: number;
  selectedOptionList: string[];
  checkMode: boolean;
};

const Question = ({
  selectOption,
  question,
  options,
  correct_answer,
  index,
  selectedOptionList,
  checkMode,
}: QuestionProps) => {
  useEffect(() => {
    if (selectedOptionList[index]) {
      selectOption(selectedOptionList[index]);
      setIsCorrectAnswer(selectedOptionList[index] === correct_answer);
    }
  }, [index]);

  const [isCorrectAnswer, setIsCorrectAnswer] = useState<boolean>(false);

  const setSelectOption = (option: string) => {
    selectOption(option);
    setIsCorrectAnswer(correct_answer === option);
  };

  return (
    <div className="questionContainer">
      <div
        className="question"
        dangerouslySetInnerHTML={{ __html: question }}
      ></div>

      <div className="options">
        {options.map((option, i) => (
          <button
            className={`button button-two option ${
              option === selectedOptionList[index] ? "active" : ""
            }`}
            key={i}
            onClick={() => setSelectOption(option)}
            dangerouslySetInnerHTML={{ __html: option }}
          ></button>
        ))}
      </div>

      {checkMode && selectedOptionList[index] && (
        <p className={`answer-status ${!isCorrectAnswer ? "incorrect" : ""}`}>
          {isCorrectAnswer
            ? "Your answer is corect"
            : `Incorrect the correct answer is ${correct_answer}`}
        </p>
      )}
    </div>
  );
};

export default Question;
