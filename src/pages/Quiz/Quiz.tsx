import "./Quiz.css";
import axios, { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import Question from "./Question/Question";
import Complete from "./Complete/Complete";

type QuizResponse = {
  response_code: number;
  results: Array<Result>;
  response_message: string;
  token: string;
};

export type Result = {
  category: string;
  type: string;
  difficulty: string;
  question: string;
  correct_answer: string;
  incorrect_answers: Array<string>;
  options: Array<string>;
};

const Quiz = () => {
  const [questionsList, setQuestionsList] = useState<Result[]>([]);
  const [activeQuestion, setActiveQuestion] = useState<number>(0);

  const [selectedOptionList, setSelectedOptionList] = useState<string[]>([]);

  const [checkMode, setCheckMode] = useState<boolean>(false);

  useEffect(() => {
    getQuizData();
  }, []);

  const handleNext = () => {
    setActiveQuestion(activeQuestion + 1);
  };
  const handleBack = () => {
    setActiveQuestion(activeQuestion - 1);
  };

  const handleSkip = () => {
    setActiveQuestion(activeQuestion + 1);
  };

  const onSetCheckmode = (value: boolean) => {
    setCheckMode(value);
    setActiveQuestion(0);
  };

  const getQuizData = () => {
    axios
      .get<QuizResponse>(
        "https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=multiple"
      )
      .then((response: AxiosResponse) => {
        setSelectedOptionList(
          Array(response.data.results.length).fill("") as string[]
        );

        setQuestionsList(() => {
          const responseData: Result[] = response.data.results.map(
            (ele: Result) => {
              const options = [...ele.incorrect_answers, ele.correct_answer];
              const shuffled = options.sort(() => 0.5 - Math.random());

              ele = { ...ele, options: shuffled };

              return ele;
            }
          );

          return responseData;
        });
      })
      .catch((error) => {
        console.error("error on open tdb :: ", error);
      });
  };

  const selectOption = (option: string) => {
    setSelectedOptionList((prev) => {
      const newSelectedOptionList = [...prev];
      newSelectedOptionList[activeQuestion] = option;
      return newSelectedOptionList;
    });
  };

  return (
    <div className="quiz-container">
      {questionsList.length === 0 ? (
        <div>Loading...</div>
      ) : (
        <div>
          {activeQuestion < questionsList.length ? (
            <Question
              selectOption={selectOption}
              question={`${activeQuestion + 1}. ${
                questionsList[activeQuestion].question
              }`}
              options={questionsList[activeQuestion].options}
              correct_answer={questionsList[activeQuestion].correct_answer}
              index={activeQuestion}
              selectedOptionList={selectedOptionList}
              checkMode={checkMode}
            />
          ) : (
            <Complete
              setCheckMode={onSetCheckmode}
              selectedOptionsList={selectedOptionList}
              questionList={questionsList}
            />
          )}

          <div className="stepper-controls">
            <div className="left">
              {activeQuestion > 0 && (
                <button className="button" onClick={handleBack}>
                  Back
                </button>
              )}
            </div>
            <div className="right">
              {activeQuestion < questionsList.length && (
                <>
                  <button className="button" onClick={handleSkip}>
                    Skip
                  </button>

                  <button
                    className="button button-two"
                    onClick={handleNext}
                    disabled={!selectedOptionList[activeQuestion]}
                  >
                    Next
                  </button>
                </>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Quiz;
