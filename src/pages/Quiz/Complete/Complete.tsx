import './Complete.css';
import { useEffect, useState } from "react";
import { Result } from "../Quiz";

interface CompleteProps {
  setCheckMode: (value: boolean) => void;
  selectedOptionsList: string[];
  questionList: Result[];
}

const Complete = ({
  setCheckMode,
  selectedOptionsList,
  questionList,
}: CompleteProps) => {
  const [correctAnswerCount, setCorrectAnswerCount] = useState<number>(0);

  useEffect(() => {
    let correctAnsCount: number = 0;

    for (let i = 0; i < selectedOptionsList.length; i++) {
      if (selectedOptionsList[i] === questionList[i].correct_answer) {
        correctAnsCount++;
      }
    }

    setCorrectAnswerCount(correctAnsCount);
  }, []);

  const handleCheckAnswers = () => {
    setCheckMode(true);
  };

  return (
    <div className='complete-page'>

      <h3>
        You got {correctAnswerCount} / {questionList.length} correct answers
      </h3>

      <button className='button button-one' onClick={handleCheckAnswers}>Check your answers</button>

    </div>
  );
};

export default Complete;
