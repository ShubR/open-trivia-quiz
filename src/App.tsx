import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Header } from "./components/Header/Header";
import Routes from "./Routes";



function App() {
  return (
    <Router>
      <Header />

      <section className="main-container">
        <Routes/>
      </section>

    </Router>
  );
}

export default App;
