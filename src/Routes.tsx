import { useRoutes, Navigate } from "react-router-dom";
import Home from "./pages/Home/Home";
import Quiz from "./pages/Quiz/Quiz";

const Routes = () => {
  return useRoutes([
    {
      path: "/",
      element: <Navigate to="/home" />,
    },
    {
      path: "/home",
      element: <Home />,
      index: true,
    },
    {
      path: "/quiz",
      element: <Quiz />,
    },
  ]);
};

export default Routes;
