import { useState, useEffect } from "react";
import "./Header.css";
import Switch from "react-switch";
import { MoonIcon, SunIcon } from "../../utils/Icons";

export const Header = () => {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("theme") === "dark") {
      setChecked(() => {
        toggleTheme(true);
        return true;
      });
    }
  }, []);

  const switchTheme = () => {
    setChecked((prev) => {
      const newValue = !prev;
      toggleTheme(newValue);
      return newValue;
    });
  };

  const toggleTheme = (darkMode: boolean) => {
    if (darkMode) {
      document.body.classList.add("dark");
      localStorage.setItem("theme", "dark");
    } else {
      document.body.classList.remove("dark");
      localStorage.setItem("theme", "light");
    }
  };

  return (
    <header>
      <nav>
        <h3>
          Open <span>Trivia Quiz</span>{" "}
        </h3>

        <Switch
          onChange={switchTheme}
          checked={checked}
          offColor="#FFBF71"
          onColor="#4d4275"
          checkedIcon={<MoonIcon />}
          uncheckedIcon={<SunIcon />}
        />
      </nav>
    </header>
  );
};
